-- 1
SELECT stores.name AS "Store Name", CONCAT(street, ' ', street2) AS "Store address", 
city AS "City", state AS "State", zip AS "Zip" FROM stores
JOIN address ON address.id = stores.address_id
ORDER BY stores.name;

-- 2
SELECT stores.name AS "Store Name", CONCAT('$', + SUM(purchase_items.quantity * items.price)) AS "Total sales in dollars" 
FROM stores
JOIN purchases ON stores.id = purchases.store_id
JOIN purchase_items ON purchases.id = purchase_items.purchase_id
JOIN items ON purchase_items.item_id = items.id
GROUP BY stores.name
ORDER BY stores.name;

-- 3
SELECT stores.name AS "Store Name", CONCAT(first_name, ' ', last_name) AS "Member Name"
FROM stores
 INNER JOIN customers ON stores.id = customers.store_id
ORDER BY stores.name;

-- 4
SELECT stores.name AS "Store Name", items.name AS "Item name", 
inventory.quantity AS "Quantity in this store", CONCAT('$', items.price) AS "Price per unit", 
items.notes AS "Notes about the unit"
FROM stores
JOIN inventory ON inventory.store_id = stores.id
JOIN items ON inventory.item_id = items.id
WHERE stores.name ILIKE 'Administaff%'
ORDER BY items.name;

-- 5
SELECT EXTRACT(MONTH FROM purchases.purchase_date) AS "Month of purchase", CONCAT('$', SUM(purchase_items.quantity * items.price)) AS "Total sales"
FROM stores
INNER JOIN purchases ON purchases.store_id = stores.id
INNER JOIN purchase_items ON purchase_items.purchase_id = purchases.id
INNER JOIN items ON purchase_items.item_id = items.id
WHERE stores.name ILIKE 'Administaff%'
GROUP BY "Month of purchase"
ORDER BY "Month of purchase";

-- 6
SELECT payment_types.payment_method AS "Payment method", stores.name AS "Store Name", 
COUNT(purchases.payment_type_id) AS "Count of purchases by payment method"
FROM payment_types
JOIN purchases ON payment_types.id = purchases.payment_type_id
JOIN stores ON purchases.store_id = stores.id 
WHERE stores.name ILIKE 'Benchmark%'
GROUP BY payment_types.payment_method, stores.name
ORDER BY payment_types.payment_method

-- 7
SELECT stores.name AS "Store Name", items.name AS "Item name", 
inventory.quantity AS "Quantity in this store"
FROM stores
JOIN inventory ON inventory.store_id = stores.id
JOIN items ON inventory.item_id = items.id
WHERE inventory.quantity < 10
ORDER BY stores.name;

-- 8
SELECT CONCAT(customers.first_name, ', ', customers.last_name) AS "Member Name", 
purchases.purchase_date AS "Purchase date",
items.name AS "Item purchased", payment_types.payment_method AS "Payment method",
purchase_items.quantity AS "Quantity purchased"
FROM customers
JOIN purchases ON purchases.customer_id = customers.id
JOIN purchase_items ON purchase_items.purchase_id = purchases.id
JOIN payment_types ON purchases.payment_type_id = payment_types.id
JOIN items ON purchase_items.item_id = items.id
WHERE customers.first_name = 'Alfred' AND customers.last_name = 'Poggi'
ORDER BY purchases.purchase_date DESC;

-- 9
UPDATE purchases
SET payment_type_id = (SELECT id FROM payment_types WHERE payment_types.payment_method = 'credit')
WHERE payment_type_id = (SELECT id FROM payment_types WHERE payment_types.payment_method = 'apple pay');

-- 10
DELETE FROM payment_types 
WHERE payment_types.payment_method = 'apple pay';

-- 11
INSERT INTO items (name, price, notes)
VALUES ('Frosted Flakes', 5, 'They’re Grate!');

-- 12
INSERT INTO inventory (store_id, item_id, quantity)
SELECT stores.id, items.id, 50
FROM stores, items
WHERE stores.name ILIKE '%a%'
AND items.name = 'Frosted Flakes';

-- 13
UPDATE items
SET notes = 'They’re Gr-r-reat!'
WHERE notes = 'They’re Grate!';