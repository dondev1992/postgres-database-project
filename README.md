# Postgres Project

## Setup Instructions

1. Download **project_dump.backup** file
2. After creating a new database in pgAdmin, right click on newly created database, then click `Restore`
3. Leave `Format` as "Custom or tar"
4. In the `Filename` selector, find and select **project_dump.backup**
5. Leave `Number of jobs` empty
6. For `Role Name`, click the drop-down and select **postgres**
7. Click `Restore` at the bottom to restore

## How to load your dump file using pgAdmin

1. Open the pgAdmin application. Always use the nenu shortcut to open pgAdmin, never a bookmark or link. pgAdmin server may not authenticate you otherwise.
2. After you have pgAdmin loaded in the browser, enter your password.
3. In most cases, you will have to create a database.
   a. Right-click `Databases` and select Database.
   b. Name the database and click `Save`.
4. Once the database has been created, we can see it in pgAdmin's list of databases.
5. Right-click your newly created database and select `Restore`.
6. In the `Restore` dialog, leave the first option set to Custom or tar.
7. For filename, click the folder icon and browse to the location you are restoring from.
8. Select the file you would like to restore and click `Select`.
9. Back in the `Restore` dialog, leave the number of jobs blamk.
10. For `Role` name, make sure you select the role `postgres`. You will have to scroll to the bottom.
11. Click `Restore`.
12. pgAdmin will update the staus of the restore operation in the bottom right. If there are any issues it will alert you. Many issues will not prevent the restore from happening.
13. That's it! You should now have a database in postgreSQL. Expand the database, expand schemas, expand tables and poke around!

## Relational Databases

### Foreign Key Relationships

| Table Name     | Foreign Key (FK) Name | Table Matching FK |
| -------------- | --------------------- | ----------------- |
| customers      | store_id              | stores            |
| stores         | address_id            | address           |
| purchases      | store_id              | stores            |
| purchases      | customer_id           | customers         |
| purchases      | payment_type_id       | payment_type      |
| inventory      | store_id              | stores            |
| inventory      | item_id               | items             |
| purchase_items | item_id               | items             |
| purchase_items | purchase_id           | purchases         |

### Multiple Choice

1. Users are not allowed to add items to purchases where the amount of items bought exceeds the amount of items in the store. This is because:
   1. There is a Trigger Function preventing new purchases that exceed the current inventory.
   2. This will cause a Foreign Key Constraint Violation on the Inventory table.
   3. -- (Correct) -- There is a custom rule on the items table that prevents it from ever having a negative value for the field 'quantity'.
   4. Users are actually allowed to add those purchases.
2. Why will the following SQL statement not work in the stores database?
   > SELECT s.name AS "Store Name", a.street AS "Street Address", a.street2 AS "Suite", a.city AS "City", a.state AS "State", a.zip AS "Zip Code"
   > FROM stores s
   > INNER JOIN address a
   > ON address_id = a.id
   1. It is missing a semi-colon.
   2. Double-quotes do not work with column names with pgAdmin and postgres.
   3. -- (Correct) -- address_id is an ambiguous field because it is not qualified with a table or alias name (should be s.address_id or stores.address_id).
   4. This is a valid query that will run without error.
3. What is the relationship between the stores and customers tables?
   1. Stores is a one-to-one relationship with Customers.
   2.   -(Correct) -- Stores is a one-to-many relationship with Customers.
   3. Stores is a many-to-many relationship with Customers.
   4. Stores and Customers are unrelated tables.
4. What is the relationship between the purchases and items tables?
   1. -- (Correct) -- The tables have a many-to-many relationship with the purchase_items table acting as a mapping table.
   2. Purchases has a one-to-many relationship with items, as a single purchase can have multiple items in it.
   3. Items has a one-to-many relationship with purchases, as a single item can have multiple purchases.
   4. Purchases and Items are unrelated tables.
